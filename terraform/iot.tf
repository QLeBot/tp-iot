# aws_iot_certificate cert
# create iot certificate with active option
resource "aws_iot_certificate" "cert" {
    active = true
}


# aws_iot_policy pub-sub
# create iot policy for pub-sub
resource "aws_iot_policy" "pub-sub" {
    name        = "pub-sub"
    policy      = file("${path.module}/files/iot_policy.json")
}

# aws_iot_policy_attachment attachment
# attach iot policy to certificate
resource "aws_iot_policy_attachment" "attachment" {
    policy      = aws_iot_policy.pub-sub.name
    target      = aws_iot_certificate.cert.arn
}

# aws_iot_thing temp_sensor
# create iot thing
resource "aws_iot_thing" "temp_sensor" {
    name = "temp_sensor"
}

# aws_iot_thing_principal_attachment thing_attachment
# attach certificate to iot thing
resource "aws_iot_thing_principal_attachment" "thing_attachment" {
    principal   = aws_iot_certificate.cert.arn
    thing       = aws_iot_thing.temp_sensor.name
}

# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py
data "aws_iot_endpoint" "iot_endpoint" {
    endpoint_type = "iot:Data-ATS"
}

# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "rule" {
    name = "rule"
    description = "test rule"
    enabled = true
    sql  = "SELECT * FROM 'sensor/temperature/+' where temperature >= 40"
    sql_version = "2016-03-23"
        dynamodbv2 {
            #hash_key_field = "id"
            #hash_key_value = "1"
            #hash_key_type = "STRING"
            #table_name = "Temperature"
            role_arn = aws_iam_role.iot_role.arn
            #operation = "INSERT"
            put_item {
                table_name = "Temperature"
            }
            #table_name = "Temperature"
        }
}

# aws_iot_topic_rule rule for sending valid data to Timestream
resource "aws_iot_topic_rule" "rule2" {
    name = "rule2"
    description = "test rule"
    enabled = true
    sql  = "SELECT * FROM 'sensor/temperature/+'"
    sql_version = "2016-03-23"
    timestream {
        role_arn = aws_iam_role.iot_role.arn
        database_name = "temperaturesensor"
        table_name = "temperaturesensor"
        dimension {
            name  = "sensor_id"
            value = "$${sensor_id}"
        }
        dimension {
            name  = "temperature"
            value = "$${temperature}"
        }
        dimension {
            name  = "zone_id"
            value = "$${zone_id}"
        }
        timestamp {
            unit  = "MILLISECONDS"
            value = "$${timestamp()}"
        }
    }
}


###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
