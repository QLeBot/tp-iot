# aws_dynamodb_table
#create aws_dynamodb_table named Temperature with a hash key
resource aws_dynamodb_table Temperature {
    name           = "Temperature"
    hash_key       = "id"
    read_capacity  = 5
    write_capacity = 5
    attribute {
        name = "id"
        type = "S"
    }
}