# aws_timestreamwrite_database "iot"
# create aws_timestreamwrite_database "iot"
resource aws_timestreamwrite_database iot {
    database_name = "iot"
    tags = {
        Name = "iot"
    }
}

# create aws_timestreamwrite_table "iot"
resource aws_timestreamwrite_table temperaturesensor {
    database_name = aws_timestreamwrite_database.iot.database_name
    table_name    = "temperaturesensor"
    retention_properties {
        magnetic_store_retention_period_in_days = 730
        memory_store_retention_period_in_hours  = 24
    }
    tags = {
        Name = "temperaturesensor"
    }
}


# aws_timestreamwrite_table linked to the database
resource aws_timestreamwrite_table Temperature {
    database_name = aws_timestreamwrite_database.iot.database_name
    table_name    = "Temperature"
    retention_properties {
        magnetic_store_retention_period_in_days = 730
        memory_store_retention_period_in_hours  = 24
    }
    tags = {
        Name = "Temperature"
    }
}


